Description: cherry pick upstream commit: ssh-native: replace EVP_Cipher
 fix the bug of ssh login
Upstream-issue: https://github.com/mytbk/fqterm/issues/35
Origin: upstream (https://github.com/mytbk/fqterm/commit/634b9612179c289c9f8c7ed18b25a0439a598131)
Forwarded: not-needed
Reviewed-By: xiao sheng wen <atzlinux@sina.com>
Last-Update: 2023-07-26

---

--- fqterm-0.9.10.1.orig/src/protocol/internal/ssh_evp_cipher.c
+++ fqterm-0.9.10.1/src/protocol/internal/ssh_evp_cipher.c
@@ -20,7 +20,13 @@ cipher_init(SSH_CIPHER* my, const uint8_
 static int
 do_crypt(SSH_CIPHER* my, const uint8_t* in, uint8_t* out, size_t l)
 {
-	return EVP_Cipher(((struct evp_priv*)my->priv)->ctx, out, in, l);
+	int crypt_bytes;
+	int res = EVP_CipherUpdate(((struct evp_priv*)my->priv)->ctx, out, &crypt_bytes, in, l);
+	if (res == 0) {
+		// failed
+		return 0;
+	}
+	return EVP_CipherFinal_ex(((struct evp_priv*)my->priv)->ctx, out, &crypt_bytes);
 }
 
 static void
