Description: update SMTH bbs hostname
 The SMTH bbs site's hostname had changed.
.
Author: xiao sheng wen <atzlinux@sina.com>
Forwarded: https://github.com/mytbk/fqterm/issues/37
Last-Update: 2023-11-16

---

--- fqterm-0.9.10.1.orig/res/userconf/address.cfg.orig
+++ fqterm-0.9.10.1/res/userconf/address.cfg.orig
@@ -1,5 +1,5 @@
 [bbs 0]
-addr=bbs.newsmth.org
+addr=bbs.mysmth.net
 alwayshighlight=0
 ansicolor=1
 antiidlestring=^@
@@ -50,7 +50,7 @@ termtype=vt102
 user=
 
 [bbs 1]
-addr=newsmth.net
+addr=www.mysmth.net
 alwayshighlight=0
 ansicolor=1
 antiidlestring=^@

--- fqterm-0.9.10.1.orig/res/userconf/fqterm.cfg.orig
+++ fqterm-0.9.10.1/res/userconf/fqterm.cfg.orig
@@ -53,7 +53,7 @@ wordwrap=72
 xim=0
 
 [quick 0]
-addr=newsmth.net
+addr=bbs.mysmth.net
 port=22
 protocol=1

--- fqterm-0.9.10.1.orig/src/common/fqterm_param.cpp
+++ fqterm-0.9.10.1/src/common/fqterm_param.cpp
@@ -26,7 +26,7 @@ namespace FQTerm {
 
 FQTermParam::FQTermParam() {
   name_ = "NEW SMTH";
-  hostAddress_ = "newsmth.net";
+  hostAddress_ = "mysmth.net";
   port_ = 23;
   hostType_ = 0; // 0--BBS 1--*NIX
   isAutoLogin_ = false;

--- fqterm-0.9.10.1.orig/src/fqterm/fqterm_screen.cpp
+++ fqterm-0.9.10.1/src/fqterm/fqterm_screen.cpp
@@ -1585,7 +1585,7 @@ void FQTermScreen::updateFixedPitchInfo(
   QString cnTestString = QString::fromUtf8("\xe5\x9c\xb0\xe6\x96\xb9\xe6\x94\xbf\xe5\xba\x9c");
   cnFixedPitch_ = (QFontMetrics(cnFont).width(cnTestString) == 
     cnTestString.length() * QFontMetrics(cnFont).width(cnTestString.at(0)));
-  QString enTestString = QString::fromUtf8("www.newsmth.net");
+  QString enTestString = QString::fromUtf8("www.mysmth.net");
   enFixedPitch_ = QFontInfo(enFont).fixedPitch() &&
                   (QFontMetrics(enFont).width(enTestString) == enTestString.length() * QFontMetrics(enFont).width(enTestString.at(0)));
                   
--- fqterm-0.9.10.1.orig/src/terminal/fqterm_buffer.cpp
+++ fqterm-0.9.10.1/src/terminal/fqterm_buffer.cpp
@@ -432,7 +432,7 @@ void FQTermBuffer::moveCaretOffset(int c
     // found caret_.column_ is out of bounds in other case, we should
     // correct it first.
     if (is_bbs_) {
-      // but the BBS (newsmth.net) assumes that the caret could be
+      // but the BBS (mysmth.net) assumes that the caret could be
       // located out of the screen :(
     } else {
       caret_.column_ = num_columns_ - 1;

diff --git a/doc/Quickstart.md b/doc/Quickstart.md
index 57076f7..cd4d551 100644
--- a/doc/Quickstart.md
+++ b/doc/Quickstart.md
@@ -5,14 +5,14 @@ FQTerm是一个易用的BBS客户端，以下将演示用FQTerm连接BBS的方
 **快速连接**是连接BBS的常用方式。快速连接的快捷键为```F3```.
 
 按下```F3```后，FQTerm会弹出一个快速连接窗口，此时需要填写的内容如下:
-- 主机名: 需要连接的服务器，如*newsmth.net*(水木社区),*bdwm.net*(北大未名)
+- 主机名: 需要连接的服务器，如*mysmth.net*(水木社区),*bdwm.net*(北大未名)
   等等，也可以填IP地址，如*162.105.205.23*(北大未名的IP地址)
 - 协议: FQTerm支持Telnet和SSH协议。几乎所有BBS都支持Telnet.现在有不少
   BBS都支持SSH协议，为了更高的安全性，建议选择SSH.
 - 端口: Telnet和SSH默认端口分别为23和22. 如果远程主机没使用默认端口，
   则需要勾选**端口**，并填写端口号。
 
-以登录水木社区为例，我们在主机名处填写*newsmth.net*,协议选*SSH*,
+以登录水木社区为例，我们在主机名处填写*mysmth.net*,协议选*SSH*,
 端口默认，点击**连接**后，填用户名和密码便登上了水木社区。
 
 ### BBS的操作
